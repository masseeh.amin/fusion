#!/bin/python
# python2.7

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
import fnmatch
import os

from calc_scores import calc_scores
import numpy as np
from torch.utils.data import Dataset
from load_features import load_all
from torch.utils.data import DataLoader
from sklearn.preprocessing import StandardScaler

class GenericData(Dataset):
    def __init__(self, x, y):
        self.X = x
        self.y = y
    def __getitem__(self, index):
        x = []
        for i in range(len(self.X)):
            x.append(self.X[i][index,:])
        return x , self.y[0][index,:]

    def __len__(self):
        return len(self.y[0])

class GenericSequenceData(Dataset):
    def __init__(self, x, y, win, speaker):
        self.X = x
        self.y = y
        seq = []
        chunk = int(len(self.y[0])/speaker)
        for s in range(0,speaker):
            seq += create_seq(range(s*chunk, (s+1)*chunk), win)

        self.seq = seq

        assert len(self.y[0]) == len(self.seq)

    def __getitem__(self, index):
        x = []
        for i in range(len(self.X)):
            x.append(self.X[i][self.seq[index]])
        return x , self.y[0][index, :]

    def __len__(self):
        return len(self.seq)

class TEST(Dataset):
    def __init__(self):
        self.x = list(range(20))
        self.y = list(range(20))

    def __getitem__(self, index):
        return self.x[index], self.y[index]

    def __len__(self):
        return len(self.x)


def read_data(paths, random_seed, normalized=False, delay=2.8, shuffle=True, valid_test=(1,1), cross_validation=False):

    base_path = "/export/livia/home/vision/masih/Recola/"    
    path_feature = [base_path + p + "arousal/" for p in paths]
    path_labels = base_path + "ratings_gold_standard/"

    # Compensate the delay (quick solution)
    shift = int(np.round(delay/0.04))
    shift = np.ones(len(path_feature),dtype=int)*shift
    scaler = []

    files_dev = fnmatch.filter(os.listdir(path_feature[0]), "new_dev_*.csv")
    files_train =  fnmatch.filter(os.listdir(path_feature[0]), "new_train_*.csv")
    files_all = files_train + files_dev

    total_speakers = len(files_all)

    if normalized:
        for i in range(0, len(path_feature)):
            scaler.append(StandardScaler())

    if shuffle == True:
        np.random.seed(1)
        np.random.shuffle(files_dev)
        np.random.shuffle(files_all)          
    
    if cross_validation:
        valid_s , test_s = valid_test
        train_s = total_speakers - valid_s - test_s

        assert (valid_s < 10 and valid_s >= 0), "[!] valid_s should be between [0,9]"
        assert (test_s < 10 and test_s >= 0), "[!] test_s should be between [0,9]"
        assert (total_speakers % valid_s == 0 ), "number of valid and test should be divideable by {}".format(total_speakers)

        number_folds = int(total_speakers/valid_s)

        X = load_all( files_all, path_feature, shift)
        y = load_all( files_all, [ path_labels ])

        one_speaker_data = int(len(X[0])/total_speakers)

        for f in range(number_folds):

            x_test = [ X[i][one_speaker_data*f:one_speaker_data*(test_s + f), :] for i in range(len(X)) ]
            y_test = [ y[0][one_speaker_data*f:one_speaker_data*(test_s + f), :] ]

            if f == number_folds - 1:   
                x_dev = [ X[i][0:one_speaker_data*valid_s, :] for i in range(len(X)) ]
                y_dev = [ y[0][0:one_speaker_data*valid_s, :] ]
                x_train_1 = [ X[i][one_speaker_data*valid_s:one_speaker_data*f, :] for i in range(len(X)) ] 
                y_train_1 = [ y[0][one_speaker_data*valid_s:one_speaker_data*f, :] ]
            else:
                x_dev = [ X[i][one_speaker_data*(f + test_s):one_speaker_data*(f + test_s + valid_s), :] for i in range(len(X)) ]
                y_dev = [ y[0][one_speaker_data*(f + test_s):one_speaker_data*(f + test_s + valid_s), :] ]
                x_train_1 = [ X[i][0:one_speaker_data*f, :] for i in range(len(X)) ] 
                y_train_1 = [ y[0][0:one_speaker_data*f, :] ]


            x_train_2 = [ X[i][one_speaker_data*(f + test_s + valid_s):, :] for i in range(len(X)) ] 
            y_train_2 = [ y[0][one_speaker_data*(f + test_s + valid_s):, :] ]
            x_train = [ np.append(x_train_1[i], x_train_2[i], axis=0) for i in range(len(X))]
            y_train = [ np.append(y_train_1[0], y_train_2[0], axis=0) ]            
            
            if normalized:
                for i in range(len(X)):
                    scaler[i].fit(x_train[i])
                    x_train[i] = scaler[i].transform(x_train[i])
                    x_dev[i] = scaler[i].transform(x_dev[i])
                    x_test[i] = scaler[i].transform(x_test[i])

            yield (x_train, y_train, x_dev, y_dev, x_test, y_test)
    
    else:
        x_train = load_all( files_train, path_feature, shift)
        y_train = load_all( files_train, [ path_labels ])

        x_dev = load_all( files_dev, path_feature, shift)
        y_dev = load_all( files_dev, [ path_labels ])
    
        if normalized:
            for i in range((len(x_train))):
                scaler[i].fit(x_train[i])
                x_train[i] = scaler[i].transform(x_train[i])
                x_dev[i] = scaler[i].transform(x_dev[i])

        yield (x_train, y_train, x_dev, y_dev, None, None)

def get_train_valid_test_loader(features,
                           batch_size=256,
                           valid_test_batch_size=1000,
                           random_seed=1,
                           valid_test=(1,1),
                           normalized=True,
                           shuffle=True,
                           win=1,
                           delay=2.8,
                           cross_validation=False,
                           **kwargs):

    assert (win > 0), "[!] windows should not be negetive"    

    read_data_gen = read_data(features, random_seed, delay=delay, valid_test=valid_test,
                                                    normalized=normalized, shuffle=shuffle, cross_validation=cross_validation)

    for X, y, x_dev, y_dev, x_test, y_test in read_data_gen:

        valid_s , test_s = valid_test
        train_s = 18 - valid_s - test_s

        if win > 1:
            if cross_validation:
                train_loader = DataLoader(dataset=GenericSequenceData(X, y, win, speaker=train_s), 
                                batch_size=batch_size, shuffle=shuffle,
                                **kwargs)

                valid_loader = DataLoader(dataset=GenericSequenceData(x_dev, y_dev, win, speaker=valid_s), 
                                batch_size=valid_test_batch_size, shuffle=False, 
                                **kwargs)
                test_loader = DataLoader(dataset=GenericSequenceData(x_test, y_test, win, speaker=test_s), 
                                batch_size=valid_test_batch_size,  shuffle=False,
                            **kwargs)
            else:
                train_loader = DataLoader(dataset=GenericSequenceData(X, y, win, speaker=9), 
                                batch_size=batch_size, shuffle=shuffle,
                                **kwargs)

                valid_loader = DataLoader(dataset=GenericSequenceData(x_dev, y_dev, win, speaker=9), 
                                batch_size=valid_test_batch_size, shuffle=False, 
                                **kwargs)

                test_loader = DataLoader(dataset=GenericSequenceData(x_test, y_test, win, speaker=9), 
                                batch_size=valid_test_batch_size,  shuffle=False,
                                **kwargs)
                  
        else:
            train_loader = DataLoader(dataset=GenericData(X, y), 
                            batch_size=batch_size, shuffle=shuffle,
                            **kwargs)

            valid_loader = DataLoader(dataset=GenericData(x_dev, y_dev), 
                            batch_size=valid_test_batch_size, shuffle=False, 
                            **kwargs)
            if cross_validation:
                test_loader = DataLoader(dataset=GenericData(x_test, y_test), 
                            batch_size=valid_test_batch_size,  shuffle=False,
                            **kwargs)
            else:
                test_loader = DataLoader(dataset=GenericData(x_dev, y_dev), 
                            batch_size=valid_test_batch_size,  shuffle=False,
                            **kwargs)

        yield (train_loader, valid_loader, test_loader)

def create_seq(l, win):
    l = list(l)

    lpadded = win // 2 * [l[0]] + l + win // 2 * [l[-1]]
    out = [lpadded[i:(i + win)] for i in range(len(l))]

    assert len(out) == len(l)
    return out

def main():

    #(67509, 88) (67509, 316) (67509, 84)

    gen = get_train_valid_test_loader(["features_audio/", "features_video_geometric/" 
                                                                            , "features_video_appearance/"], valid_test=(1,1),
                                                                            cross_validation=True)

    for train_loader, valid_loader, test_loader in gen :

        print(len(train_loader.dataset), len(valid_loader.dataset), len(test_loader.dataset))
    

if __name__ == '__main__':
    main()
