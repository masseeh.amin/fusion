import numpy as np
from scipy import signal

def get_std_ratio(label, pred):
    std_l = np.std(label, axis=0)
    std_t = np.std(pred, axis=0)
    return std_l/std_t

def get_mean_dif(label, pred):
    label_mean = np.mean(label, axis=0)
    pred_mean = np.mean(pred, axis=0)
    return label_mean - pred_mean

def get_min_max(label):
    return (np.amin(label, axis=0), np.amax(label, axis=0))

def min_max_scaling(X, feature_range):
    min, max = feature_range
    X_std = (X - X.min(axis=0)) / (X.max(axis=0) - X.min(axis=0))
    X_scaled = X_std * (max - min) + min
    return X_scaled

def apply_scaling(pred, ratio):
    return np.asarray([x*ratio for x in pred])

def mean_centering(pred, ratio):
    return np.asarray([x + ratio for x in pred])

def post_process_pipline(prediction_val, ratio, window=123):

    prediction_val = min_max_scaling(prediction_val, ratio[2])
    prediction_val = mean_centering(prediction_val, ratio[1])    
    # prediction_val = apply_scaling(prediction_val, ratio[0])

    if prediction_val.shape[1] == 2:
        pv1 = signal.medfilt(prediction_val[:,0], window).reshape(-1,1)
        pv2 = signal.medfilt(prediction_val[:,1], window).reshape(-1,1)
        prediction_val = np.concatenate((pv1, pv2), axis=1)
    else:
        prediction_val = signal.medfilt(prediction_val[:,0], window).reshape(-1,1)

    
    return prediction_val