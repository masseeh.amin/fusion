#!/bin/python
# python2.7

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

def get_hyper_param(hyper_param_optimiztion, default_params):
    from sklearn.model_selection import ParameterGrid

    args = default_params['args']
    name = default_params['model-name']

    if hyper_param_optimiztion:
        space = {
            'lr' : [args.lr],
            'epochs' : [args.epochs],
            'batch_size' : [args.batch_size],
            'test_batch_size' : [args.test_batch_size],
            'seed' : [args.seed, 62, 99, 142, 13, 64],
            # 'seed' : [142],
            'patience': [args.patience],
            'win': [1],
            'delay': [2.8],
            'name': [name],
            'mode': [args.mode],
            'opt_algo': [args.opt],
            'cv': [args.cv],
            # 'fusion_layers': [
            #     # [ ([audio, video], drop_out), ([audio, video], drop_out) ]
            #     [ ([150, 150], 0.2), ([50, 50], 0.4) ],
            #     [ ([100, 200], 0.2), ([50, 50], 0.4) ],
            #     [ ([200, 100], 0.2), ([50, 50], 0.4) ],                
            #     [ ([100, 300], 0.2), ([100, 50], 0.4) ],
            # ]
            'fusion_layers': [
                # [ ([audio, video], drop_out), ([audio, video], drop_out) ]
                [ ([150, 150], 0.2), ([50, 50], 0.4) ]
            ]
        }

        return ParameterGrid(space)

    else:
        space = {
            'lr' : args.lr,
            'epochs' : args.epochs,
            'batch_size' : args.batch_size,
            'test_batch_size' : args.test_batch_size,
            'seed' : args.seed,
            'patience': args.patience,
            'win': 1,
            'delay': args.delay,
            'name': name,
            'mode': args.mode,
            'opt_algo': args.opt,
            'cv': args.cv,
            'fusion_layers': [ ([150, 150], 0.2), ([100, 100], 0.4) ]
        }

        return [space]
