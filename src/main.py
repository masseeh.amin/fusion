#!/bin/python
# python2.7

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import pprint
import sys
import fnmatch
import os
import argparse
import time
from calc_scores import calc_scores
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
from dataloader import get_train_valid_test_loader as load_data
from progbar import Progbar
from hyper_params import get_hyper_param
import networks
import pickle
import post_processing as pp
import json

# Training settings
parser = argparse.ArgumentParser(description='Fusion')
parser.add_argument('--mode', type=int, default=0, metavar='N',
                    help='arousal = 0, valence = 1')
parser.add_argument('--model', type=str, default="", metavar='N',
                    help='audio, video, early, fusion, late, fusion_sep')
parser.add_argument('--opt', type=str, default="sgd", metavar='N',
                    help='adam, sgd, rmsprop')
parser.add_argument('--delay', type=float, default=2.8, metavar='N',
                    help='delay')
parser.add_argument('--patience', type=int, default=-1, metavar='N',
                    help='patience for early stopping, -1 for disabling')
parser.add_argument('--batch-size', type=int, default=256, metavar='N',
                    help='input batch size for training (default: 64)')
parser.add_argument('--test-batch-size', type=int, default=1000, metavar='N',
                    help='input batch size for testing (default: 1000)')
parser.add_argument('--epochs', type=int, default=50, metavar='N',
                    help='number of epochs to train (default: 50)')
parser.add_argument('--lr', type=float, default=0.01, metavar='LR',
                    help='learning rate (default: 0.01)')
parser.add_argument('--momentum', type=float, default=0.9, metavar='M',
                    help='SGD momentum (default: 0.5)')
parser.add_argument('--no-cuda', action='store_true', default=False,
                    help='disables CUDA training')
parser.add_argument('--save', action='store_true', default=False,
                    help='save model')
parser.add_argument('--save-params', action='store_true', default=False,
                    help='save model parameters')
parser.add_argument('--graph', action='store_true', default=False,
                    help='save graph')
parser.add_argument('--hpo', action='store_true', default=False,
                    help='hyper parameters optimization')
parser.add_argument('--verbose', action='store_true', default=False,
                    help='show detailed output')
parser.add_argument('--cv', action='store_true', default=False,
                    help='cross validation')
parser.add_argument('--seed', type=int, default=10, metavar='S',
                    help='random seed (default: 1)')
parser.add_argument('--log-interval', type=int, default=50, metavar='N',
                    help='how many batches to wait before logging training status')


args = parser.parse_args()
args.cuda = not args.no_cuda and torch.cuda.is_available()

kwargs = {'num_workers': 1, 'pin_memory': True} if args.cuda else {'num_workers': 2, 'pin_memory': False}

models_dic = {
    "1": "early",
    "2": "fusion",
    "3": "audio",
    "4": "video",
    "5": "fusion_sep",
    "6": "late",
    "7": "fusion_early"
}

if args.model == '':
    model_name = models_dic["7"]
else:
    model_name = args.model 

fn_loss = F.mse_loss

def to_var(x, volatile):
    if args.cuda:
        x = x.cuda()
    return Variable(x, volatile=volatile)

def denorm(x):
    out = (x + 1) / 2
    return out.clamp(0, 1)

def get_modal(data, target, volatile):
    data[0] = data[0].view(len(data[0]),-1)
    data[1] = data[1].view(len(data[1]),-1)
    data[2] = data[2].view(len(data[2]),-1)

    audio = to_var(data[0], volatile)
    video_geo = to_var(data[1], volatile)
    video_ap = to_var(data[2], volatile)

    video_merge = to_var(torch.cat(seq=(data[1], data[2]), dim=1), volatile)

    early_fusion_features = to_var(torch.cat(seq=(data[0], data[1], data[2]), dim=-1), volatile)

    if args.mode == 0 or args.mode == 1:
        t = to_var(target[:, args.mode:args.mode+1], volatile)
    else:
        t = to_var(target, volatile)

    output_dic = {
        'audio' : audio,
        'video' : video_merge,
        'video_geo': video_geo,
        'video_ap': video_ap,
        'fusion': [audio, video_merge],
        'fusion_early': [audio, video_merge],        
        'late': [audio, video_merge],
        'fusion_sep' : [audio, video_geo, video_ap],
        'early' : early_fusion_features
    }

    return output_dic[model_name] , t

def select_model(name, win=1, layers=[
                                    ([150, 150], 0.2),
                                    ([50, 50], 0.4)
                                ]):
    if name == 'audio':
        return networks.UniModalNet(win*88, hidden=[(100, 0.2), (100, 0.6)])
    elif name == 'video':  
        return networks.UniModalNet(win*2*(84 + 316), hidden=[(300, 0.2), (50, 0.6)])
    elif name == 'video_geo':  
        return networks.UniModalNet(win*2*316, hidden=[(300, 0.2), (50, 0.6)])
    elif name == 'fusion':  
        return networks.FusionNet([win*88, win*2*(316 + 84)] , layers, mode='default')
    elif name == 'fusion_early':  
        return networks.FusionNet([win*88, win*2*(316 + 84)] , [
                                    ([150, 150], 0.2),
                                    ([50, 50], 0.4)
                                ], mode='early')
    elif name == 'late':  
        return networks.FusionNet([win*88, win*2*(316 + 84)], [ 
                                    ([150, 150], 0.2),
                                    ([50, 50], 0.4) 
                                ], mode='late')
    elif name == 'fusion_sep':  
        return networks.FusionNet([win*88, win*2*316, win*2*84], [
                                    ([100, 400, 100], 0.2)
                                ])
    elif name == 'early':  
        return networks.UniModalNet((88 + 84*2 + 316*2)*win, hidden=[(300, 0.2), (100, 0.4)])

def select_optimizer(optim_algo, lr, model_params):

    alpha_params = []
    rest_params = []
    for name, param in model_params:
        if 'alpha' in name:
            alpha_params.append(param)
        else:
            rest_params.append(param)

    if optim_algo == 'sgd':
        per_param = [
                        {'params': alpha_params, 'lr': lr*10},
                        {'params': rest_params}
                    ]
        return optim.SGD(per_param, lr=lr, momentum=0.9, nesterov=True)
    elif optim_algo == 'adam':
        per_param = [
                        {'params': alpha_params, 'lr': 0.001},
                        {'params': rest_params}
                    ]
        return optim.Adam(per_param, lr=0.01)
    elif optim_algo == 'rmsprop':
        per_param = [
                        {'params': alpha_params, 'lr': 0.1},
                        {'params': rest_params}
                    ]
        return optim.RMSprop(per_param, lr=lr*0.01, momentum=0.9)

def per_epoch(phase, model, data_loader, optimizer=None, model_state=None, stats={}):

    if phase == 'train':
        inference = False
        model.train()
        progbar = Progbar(len(data_loader.dataset))        
    elif phase == 'valid':
        inference = True
        model.eval()
    elif phase == 'test':
        inference = True        
        model.eval()
        model.load_state_dict(model_state['config'])
    
    total_loss = 0
    total_target = []
    total_output = []

    for batch_idx, (data, target) in enumerate(data_loader):
        input, target = get_modal(data, target, volatile=inference)
        total_target.append(target.cpu().data.numpy())

        if phase == 'train':
            optimizer.zero_grad()
        
        output = model(input)   
        total_output.append(output.cpu().data.numpy())             
        loss = fn_loss(output, target)

        if phase == 'train':
            loss.backward()
            optimizer.step()
            progbar.add(len(data[0]), values=[("train loss", loss.data[0])])
            
        total_loss += loss.data[0]
    
    total_loss /= len(data_loader)
    total_target = np.concatenate(total_target, axis=0)
    total_output = np.concatenate(total_output, axis=0)

    if phase == 'train':
        stats["loss"].append(round(total_loss, 4))
        progbar.reset()    
        ratio = []
        ratio.append(pp.get_std_ratio(total_target, total_output))
        ratio.append(pp.get_mean_dif(total_target, total_output))
        ratio.append(pp.get_min_max(total_target))

        return ratio

    else:
        total_output = pp.post_process_pipline(total_output, window=123, ratio=model_state['ratio'])
        ccc = calc_scores(total_target, total_output)
        
        if phase == 'valid':  
            stats["loss"].append(round(total_loss, 4))
            stats["ccc"].append(round(ccc[0], 4))

            print('Validation set Average loss: {:.4f}\tCCC : {}'.format(total_loss, ccc))

            return ccc[0] , total_loss
        
        elif phase == 'test':

            stats["results"]["label"] = total_target[0:9*7501,:]
            stats["results"]["pred"] = total_output

            print('Test set Average loss: {:.4f}\tCCC : {}'.format(total_loss, ccc))

            return ccc[0] , total_loss

    return

def fit(params):

    torch.manual_seed(params['seed'])
    if args.cuda:
        print("Running in GPU mode")
        torch.cuda.manual_seed(params['seed'])

    print("Training model Params : {}".format(params))

    features = ["features_audio/", "features_video_geometric/", "features_video_appearance/"]
    load_data_gen = load_data(features,
                                batch_size=params['batch_size'],
                                valid_test_batch_size=params['test_batch_size'],
                                random_seed=params['seed'],
                                normalized=True,
                                valid_test=(1,1),
                                shuffle=True,
                                win=params['win'],
                                delay=params['delay'],
                                cross_validation=params['cv'],
                                **kwargs)
    
    cross_validation_results = []

    average_stats = {
        'test': {
                'loss': [],
                'ccc': []
        },
        'valid': {
                'loss': [],
                'ccc': []
        }
    }
    attachments = {
            'best_model_params_ccc': []
    }

    start_time = time.time()

    for train_loader, valid_loader, test_loader in load_data_gen:

        model = select_model(name=params['name'], layers=params['fusion_layers'])

        graph = str(model)
        print(graph)
        if args.cuda:
            model.cuda()

        optimizer = select_optimizer(params['opt_algo'], params['lr'], model.named_parameters())

        scheduler = optim.lr_scheduler.StepLR(optimizer, step_size=5, gamma=0.5)
        reduceLROnPlateau = optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'min', verbose=True)    

        stats = {
            "train": {
                "loss": []
            },
            "valid": {
                "loss": [],
                "ccc": []
            },
            "test": {
                "results" : {
                    "label" : [],
                    "pred" : []
                }
            }
        }

        patience = 0
        best_ratio = 0
        best_valid_loss = np.inf
        best_valid_ccc = -np.inf
        best_valid_epoch_loss = 0
        best_valid_epoch_ccc = 0    
        best_model_params_ccc = model.state_dict()
        best_model_inter = model.alpha_params()
        best_model_graph = model.to_json()
        alpha_hist = []

        for epoch in range(1, params['epochs'] + 1):
            print('Epoch: {}/{}'.format(epoch, args.epochs))
            if args.verbose:
                pprint.pprint(model.alpha_params())
            
            scheduler.step()

            alpha_hist.append(model.alpha_params())
            
            ratio = per_epoch(phase='train', model=model, data_loader=train_loader, optimizer=optimizer,
                                 stats=stats['train'])

            this_valid_ccc , this_valid_loss = per_epoch(phase='valid', model=model, data_loader=valid_loader,
                                                             model_state={'ratio': ratio}, stats=stats["valid"])            
            reduceLROnPlateau.step(this_valid_loss)

            if best_valid_ccc <= this_valid_ccc:
                best_valid_ccc = this_valid_ccc
                best_valid_epoch_ccc = epoch        
                best_ratio = ratio    
                best_model_params_ccc = model.state_dict() 
                best_model_inter = model.alpha_params() 
                best_model_graph = model.to_json()                                  

            if best_valid_loss >= this_valid_loss:
                best_valid_loss = this_valid_loss
                best_valid_epoch_loss = epoch
                patience = 0            
            else:
                patience += 1
            
            if params['patience'] > 0:  #if patience is enabled
                if patience > params['patience'] :
                    break
        
        print('Best loss score on validation set : {:.4f} in Epoch {}'.format(best_valid_loss, best_valid_epoch_loss))
        print('Best CCC score on validation set: {:.4f} in Epoch {}'.format(best_valid_ccc, best_valid_epoch_ccc))
        pprint.pprint(best_model_inter)

        test_ccc, test_loss = per_epoch(phase='test', model=model, data_loader=test_loader,
                                         model_state={'config':best_model_params_ccc, 'ratio':best_ratio}, stats=stats["test"])
        

        average_stats['test']['ccc'].append(test_ccc)
        average_stats['test']['loss'].append(test_loss)

        average_stats['valid']['ccc'].append(best_valid_ccc)
        average_stats['valid']['loss'].append(best_valid_loss)

        cross_validation_results.append({
            'test_ccc': test_ccc,
            'test_loss': test_loss,
            'valid_ccc': best_valid_ccc,
            'valid_loss': best_valid_loss,                        
            'best_epoch_loss' : best_valid_epoch_loss,
            'best_epoch_ccc' : best_valid_epoch_ccc,        
            'stats' : stats,
            'alphas' : alpha_hist,
            'best_model_inter': best_model_inter,
        })

        if args.save_params:
            filename = "../results/.temp/" + time.strftime("%Y%m%d-%H%M%S") + "_model_params.dat"
            fileObject = open(filename, 'wb') 
            pickle.dump(best_model_params_ccc, fileObject)
            fileObject.close()
            attachments['best_model_params_ccc'].append(filename)

        if args.graph:
            filename = "notebooks/graph/" + time.strftime("%Y%m%d-%H%M%S") + ".json"
            with open(filename, 'w') as outfile:
                json.dump(best_model_graph, outfile)
            print("fusion graph has been saved")
    

    average_test_ccc = sum(average_stats['test']['ccc'])/float(len(average_stats['test']['ccc']))
    average_test_loss = sum(average_stats['test']['loss'])/float(len(average_stats['test']['loss']))

    ccc_test_std = np.std(average_stats['test']['ccc'])
    loss_test_std = np.std(average_stats['test']['loss'])

    average_valid_ccc = sum(average_stats['valid']['ccc'])/float(len(average_stats['valid']['ccc']))

    print('Average loss score and STD on test set : {:.4f} and {:.4f}'.format(average_test_loss, loss_test_std))
    print('Average CCC score and STD on test set : {:.4f} and {:.4f}'.format(average_test_ccc, ccc_test_std))

    exec_time = time.time() - start_time
    print('Exection time: {:.4f}'.format(exec_time/60))

    return {
            'test_ccc': average_test_ccc,
            'test_loss': average_test_loss,
            'valid_ccc': average_valid_ccc,
            'loss_std': loss_test_std,
            'ccc_std': ccc_test_std,
            'cross_valid': cross_validation_results,
            'graph': graph,            
            'space': params,       
            'optimizer' : params['opt_algo'], 
            'name': params['name'],    
            'exec_time': exec_time,                        
            'attachments': attachments
        }

def save_to_file(obj, model_name):  
    mode = {
        '0': 'arousal',
        '1': 'valence',
        '3': 'both'
    }
    
    filename = time.strftime("%Y%m%d-%H%M%S") + "--" + mode[str(args.mode)] + "--" + model_name + ".dat" 
    fileObject = open("../results/" + filename,'wb') 

    pickle.dump(obj ,fileObject)   

    fileObject.close()
    print("model has been saved") 

def report(data):
    max_ccc = max(data, key=lambda item: item["valid_ccc"])

    print("Hyper parameters optimization result:")
    print('Average loss score and STD on test set: {:.4f} and {:.4f}'.format(max_ccc['test_loss'], max_ccc['loss_std']))
    print('Average CCC score and STD on test set: {:.4f} and {:.4f}'.format(max_ccc['test_ccc'], max_ccc['ccc_std']))
    print('Best parameters {}'.format(max_ccc['space']))

    if args.save:
        model_name = "model_{}_{}".format(max_ccc['name'], str(round(max_ccc['test_ccc'], 4)))
        save_to_file(data, model_name)

    return

def hyper_opt():
    space = get_hyper_param(hyper_param_optimiztion=args.hpo, default_params={'args': args, 'model-name': model_name})

    result = [fit(params) for params in space]

    report(result)



if __name__ == '__main__': 
    hyper_opt()
