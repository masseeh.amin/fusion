var width = 960,
height = 500,
nodeSize = 35;

var color = d3.scale.category20();

var svg = d3.select("body").append("svg")
.attr("width", width)
.attr("height", height);

d3.json("data.json", function(error, graph) {
	var nodes = graph.nodes;

    // get network size
    var netsize = {};
    nodes.forEach(function (d) {
    	if(d.layer in netsize) {
    		netsize[d.layer] += 1;
    	} else {
    		netsize[d.layer] = 1;
    	}
    	d["lidx"] = netsize[d.layer];
    });

    console.log(nodes)

    // calc distances between nodes
    var largestLayerSize = Math.max.apply(
    	null, Object.keys(netsize).map(function (i) { return netsize[i]; }));

    var lastLayer = Math.max.apply(
    	null, nodes.map(function (i) { return i.layer; }));

    var xdist = width / Object.keys(netsize).length,
    ydist = height / largestLayerSize;

    // create node locations
    nodes.map(function(d) {
    	d["x"] = (d.layer - 0.5) * xdist;
    	d["y"] = (d.lidx - 0.5) * ydist;
    });

    // autogenerate links
    var links = [];
    nodes.map(function(d, i) {
    	for (var n in nodes) {
    		if (d == nodes[n]) {}
    			else if (d.layer == lastLayer) {}
    					else if (d.layer == lastLayer - 1 && d.layer + 1 == nodes[n].layer) {
    						links.push({"source": parseInt(i), "target": parseInt(n), "value": 1, "alpha": d.alpha, "max":d.max}) }
    				else if (d.layer + 1 == nodes[n].layer && d.lidx == nodes[n].lidx) {
    					item = {"source": parseInt(i), "target": parseInt(n), "value": 1, "w": nodes[n].w}
    					links.push(item) 
    				}
    				else if (d.layer != lastLayer - 1 && d.layer == nodes[n].layer && d.lidx + 1 == nodes[n].lidx) {
    					links.push({"source": parseInt(i), "target": parseInt(n), "value": 1, "alpha": d.alpha || nodes[n].alpha,
    								 "max": d.max || nodes[n].max}) 
    				}
    					}
    				}).filter(function(d) { return typeof d !== "undefined"; });


    // draw links
    var link = svg.selectAll(".link")
    .data(links)
    .enter().append("line")
    .attr("class", "link")
    .attr("x1", function(d) { return nodes[d.source].x; })
    .attr("y1", function(d) { return nodes[d.source].y; })
    .attr("x2", function(d) { return nodes[d.target].x; })
    .attr("y2", function(d) { return nodes[d.target].y; })
    .style("stroke-width", function(d) { return Math.sqrt(d.value); });

    //Add the SVG Text Element to the svgContainer
    var text = svg.selectAll("text")
    .data(links)
    .enter()
    .append("text");

    //Add SVG Text Element Attributes
    var textLabels = text
    .attr("x", function(d) { if (nodes[d.target].x == nodes[d.source].x) {return nodes[d.target].x} else {return nodes[d.source].x + (nodes[d.target].x - nodes[d.source].x)/2} })
    .attr("y", function(d) { if (nodes[d.target].y == nodes[d.source].y) {return nodes[d.target].y} else {return nodes[d.source].y + (nodes[d.target].y - nodes[d.source].y)/2} })
    .text( function (d) { 
    	if (d.alpha) return "a:" + d.alpha
    	else  return "w:" + d.w
    })
    .attr("font-family", "sans-serif")
    .attr("font-size", "15px")
    .attr("fill", function (d) {
    	if(d.max == 1) return "red"
    	return "black"
    });

    // draw nodes
    var node = svg.selectAll(".node")
    .data(nodes)
    .enter().append("g")
    .attr("transform", function(d) {
    	return "translate(" + d.x + "," + d.y + ")"; }
    	);

    var circle = node.append("circle")
    .attr("class", "node")
    .attr("r", nodeSize)
    .style("fill", function(d) { return color(d.layer); });


    node.append("text")
    .attr("dx", "-1.2em")
    .attr("dy", ".35em")
    .text(function(d) { return d.label; });
});