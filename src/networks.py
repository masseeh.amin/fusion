#!/bin/python
# python2.7

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
import numpy as np
from numpy import linalg as LA
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import json

def init_value(w):
    nn.init.xavier_normal(w)
    return

class UniModalNet(nn.Module):
    def __init__(self, in_features, hidden):
        super(UniModalNet, self).__init__()

        self.net = nn.Sequential()
        for i, (hu , drop_out_rate) in enumerate(hidden):
            if i == 0:
                self.net.add_module("input", nn.Linear(in_features, hu))
            else:
                hu_prev , _ = hidden[i-1]
                self.net.add_module("fc_" + str(i), nn.Linear(hu_prev, hu))              
          
            self.net.add_module("act_" + str(i), nn.Sigmoid())
            self.net.add_module("drop_out_" + str(i), nn.Dropout(drop_out_rate))    
    
        hu , _ = hidden[-1]
        self.net.add_module("regression", nn.Linear(hu, 1))

    def forward(self, x):
        return self.net(x)

    def alpha_params(self):
        return {}

    def to_json(self):
        return {}

class FusionBlock(nn.Module):
    def __init__(self, in_features, out_features, drop_out_rate=0.2, is_predict=False, mode='default'):
        super(FusionBlock, self).__init__()
        if type(in_features) is not list or type(out_features) is not list:
            raise AttributeError("wrong inputs")
             
        self.is_predict = is_predict
        self.drop_out_rate = drop_out_rate
        self.input_size = len(in_features)
        self.linears = nn.ModuleList()
        
        for i in range(self.input_size):
            self.linears.add_module('net_' + str(i), nn.Linear(in_features[i], out_features[i]))

        fuse_input = 0
        fuse_output = 0
        for i in range(self.input_size):
            fuse_input += in_features[i]
            fuse_output += out_features[i]
        
        if self.is_predict:
            fuse_output = 1
        
        self.linears.add_module('fused' ,nn.Linear(fuse_input, fuse_output))
        
        if not self.is_predict:        
            if mode == 'default':
                self.alpha = nn.Parameter(torch.rand(self.input_size,1))
            else:
                self.alpha = Variable(torch.from_numpy(np.array([[0],[0]], np.float32)).cuda(), requires_grad=False)       
    
    #inputs is array of inputs and the last item have be input_fusion
    def forward(self, inputs):
        for i, l in enumerate(self.linears):
            inputs[i] = l(inputs[i])

        if not self.is_predict:      

            cat_seq = []
            for i in range(len(inputs) -1):
                cat_seq.append(inputs[i] * self.alpha[i]) 
            
            
            #fusing inputs using alpha
            inputs[-1] = torch.cat(cat_seq , 1) + inputs[-1]

            for i in range(len(inputs)):
                inputs[i] = F.dropout(F.sigmoid(inputs[i]), p=self.drop_out_rate, training=self.training)    
        
        return inputs

    def alpha_params(self):
        dic = {
            'alpha': [],
            'weights': []
        }

        if not self.is_predict:
            for i, a in enumerate(self.alpha):
                dic['alpha'].append(a.cpu().data.numpy()[0])
        
        for i, l in enumerate(self.linears):
            dic['weights'].append(LA.norm(l.weight.cpu().data.numpy()[0]))    

        return dic

    def to_json(self, layer, name):
        nodes = []
        for index, l in enumerate(self.linears):
            item = {
                    "label": name + "_" + str(index),
                    "layer": layer,
                    "w": round(LA.norm(l.weight.cpu().data.numpy()[0]), 3),
                }

            if index != len(self.linears) - 1 and not self.is_predict:
                item["alpha"] = round(self.alpha[index].cpu().data.numpy()[0], 5)

            nodes.append(item)
        

        if not self.is_predict:
            _, idx = torch.max(self.alpha, 0) 
            nodes[idx.cpu().data.numpy()[0]]["max"] = 1
            nodes[-1], nodes[-2] = nodes[-2], nodes[-1]

        return nodes

class FusionNet(nn.Module):
    def __init__(self, in_features, layers, mode='default'):
        super(FusionNet, self).__init__()
        if type(layers) is not list:
            raise AttributeError("wrong inputs")

        self.number_inputs = len(in_features)

        self.fusion_layers = nn.ModuleList() 
        for index, (hidden, dp_rate) in enumerate(layers):
            if index == 0:
                self.fusion_layers.add_module('fb_' + str(index), FusionBlock(in_features=in_features, out_features=hidden,
                                         drop_out_rate=dp_rate, mode=mode))
            else:
                hidden_prev , _ = layers[index-1]
                self.fusion_layers.add_module('fb_' + str(index), FusionBlock(in_features=hidden_prev, out_features=hidden,
                                         drop_out_rate=dp_rate, mode=mode))
                       

        hidden, dp_rate = layers[-1]
        self.fusion_layers.add_module('pre_reg', FusionBlock(hidden, [1]*self.number_inputs, drop_out_rate=dp_rate,
                                                     is_predict=True, mode=mode))

        self.regression = nn.Linear(self.number_inputs + 1, 1)

        if mode == 'default':
            self.alpha_input = nn.Parameter(torch.rand(self.number_inputs, 1))
        elif mode == 'early':
            self.alpha_input = Variable(torch.from_numpy(np.array([[1],[1]], np.float32)).cuda(), requires_grad=False)
        elif mode == 'late':
            self.alpha_input = Variable(torch.from_numpy(np.array([[0],[0]], np.float32)).cuda(), requires_grad=False)        
            
        if mode == 'default' or mode == 'late':
            self.alpha_out = nn.Parameter(torch.rand(self.number_inputs + 1, 1))
        elif mode == 'early':
            self.alpha_out = Variable(torch.from_numpy(np.array([[0],[0],[1]], np.float32)).cuda(), requires_grad=False)

        # init_value(self.alpha_out.data)
        
    
    def forward(self, inputs):            
        # self.alpha_input.data = (self.alpha_input / torch.sum(self.alpha_input)).data        

        cat_seq = []
        for i in range(len(inputs)):
            cat_seq.append(inputs[i] * self.alpha_input[i])

        input_fusion = torch.cat(cat_seq , 1)
        # self.alpha_input.data = F.sigmoid(self.alpha_input).data

        inputs.append(input_fusion)
        for i, f in enumerate(self.fusion_layers):
            inputs = f(inputs)

        # inputs[-1] = self.regression(F.sigmoid(inputs[-1]))
        # cat_seq = []
        # for i in range(len(inputs)):
        #     cat_seq.append(inputs[i])

        f = torch.cat(inputs , 1)    
        # f = self.regression(f)  
        # f = F.tanh(self.regression(f))                            
        
        # self.alpha_out.data = (self.alpha_out / torch.sum(self.alpha_out)).data                        

        result = 0
        for i in range(len(inputs)):
            result += inputs[i] * self.alpha_out[i]
                
        return result

    def alpha_params(self):
        dic = {
            'fusion_layers' : [],
            'inputs': [],
            'outputs': [],
        }

        for idx, i in enumerate(self.fusion_layers):
            dic['fusion_layers'].append(i.alpha_params())

        for i, a in enumerate(self.alpha_input):
            dic['inputs'].append(a.cpu().data.numpy()[0])
    
        for i, a in enumerate(self.alpha_out):
            dic['outputs'].append(a.cpu().data.numpy()[0])

        return dic

    def to_json(self):
        nodes = []

        n_input = []

        for i, a in enumerate(self.alpha_input):
            item = {
                "label": "input_" + str(i),
                "alpha":round(a.cpu().data.numpy()[0], 5),
                "layer": 1,
            }
            n_input.append(item)

        _, idx = torch.max(self.alpha_input, 0)
        n_input[idx.cpu().data.numpy()[0]]["max"] = 1            

        n_input.append({
            "label": "input_" + str(len(self.alpha_input)),
            "layer": 1,
        })

        n_input[-2], n_input[-1] = n_input[-1], n_input[-2]
        nodes.append(n_input)

        _, idx_out_max = torch.max(self.alpha_out, 0)

        for idx, i in enumerate(self.fusion_layers):
            n = i.to_json(layer=idx + 2, name="fc"+str(idx))
            if idx == len(self.fusion_layers) - 1:
                for j ,d in enumerate(n):
                    d["alpha"] = round(self.alpha_out[j].cpu().data.numpy()[0], 5)
                    if j == idx_out_max.cpu().data.numpy()[0]:
                        d["max"] = 1
                n[-1], n[-2] = n[-2], n[-1]
            nodes.append(n)

        nodes.append([{"label": "output", "layer": len(self.fusion_layers) + 2, "index": 0}])

        from itertools import chain
        nodes = list(chain(*nodes)) 

        return {"nodes": nodes}

def main():
    model = FusionNet([102, 84], [([100, 100], 0.2)])
    # print(model.alpha_params())

    x1 = Variable(torch.rand(1,102))
    x2 = Variable(torch.rand(1,84))

    out = model([x1, x2])

    r = json.dumps(model.to_json())

    print(r)
    

if __name__ == '__main__':
    main()